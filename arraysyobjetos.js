//creando un array
var a = ['one', 'two', 3, false, 'five'];
console.log(a);
console.log("La longitud del array es " + a.length);

var posicion = a[1];
console.log(posicion);

//Añadir elemento a un array
a.push('added');

//creando un objeto
var b = {fif:'one', sec:'two', th:3};
console.log(b);

//Hay dos formas de acceder a una propiedad del objeto
var propiedad = b["sec"];
var otraPropiedad = b.th;
console.log(propiedad + ' ' + otraPropiedad);

//Añadiendo propiedades al objeto
b.fou = "Hola!";
b.fiv = 777;
console.log(b);

//Actualizando las propiedades de un objeto:
b.fou = "Adios!";
console.log(b);