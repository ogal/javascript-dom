// alert('Pagina enlazada');

var x = 2; 
var y = 3;
var str = 'Los strings pueden ir con comilla simple o doble'; 

console.log(resul = x + y);

// console. dir es utilizado para objetos que tienen estructura de árbol
// console.dir(window);

// document está dentro de wondow, dentro de la página web
// console.dir(document);

//Como un alert, pero en este caso con un campo para la entrada de texto
// El objeto window normalmente no hay que ponerlo
var pregunta = 'Cual es tu nombre?';
// var response = "A la pregunta " + pregunta + ': ' + prompt(pregunta);
// console.log(response);

//operadores
var seis = 9 - 3;
console.log(seis);

var Frase = "Javascript es case sensitive.";
var frase = 'Esta no es la misma variable que Frase.';
console.log(Frase + ' ' + frase);

var x = '1' + 1;
var y = 'un string' + 'otro string';
var z = 'frase' + 1;
console.log(x + ' ' + y + ' ' + z);